#!/usr/bin/env python3

from os.path import join, dirname
from setuptools import setup, find_packages

from makesecretlib.settings import VERSION


REQUIRED_PYTHON = (3, 6)


def readme():
    with open(join(dirname(__file__), 'README.md')) as f:
        return f.read()


setup(
    name='makesecretlib',
    version=VERSION,
    python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
    description='A backend library for makesecret and makesecret-qt.',
    long_description=readme(),
    long_description_content_type='text/markdown',
    author='Raymond L. Rivera',
    author_email='ray.l.rivera@gmail.com',
    maintainer='Raymond L. Rivera',
    maintainer_email='ray.l.rivera@gmail.com',
    url='https://gitlab.com/ghost-in-the-zsh/makesecretlib',
    packages=find_packages(),
    #
    # FIXME: Find a way to get clients to access this as shared data from
    #        this lib; currently clients end up with wrong search path, so
    #        they have their own local copies, for now. We maintain a copy
    #        in this repo to allow unit tests to pass.
    #
    # data_files=[
    #     ('data', ['makesecretlib/data/eff_wordlist.db'])
    # ],
    install_requires=[
        'requests'
    ],
    include_package_data=True,
    keywords='secret password passphrase security cryptography entropy generator verification library',
    classifiers=(
        # https://pypi.org/classifiers/
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 Only',
        'Topic :: Security',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Security :: Cryptography',
    ),
)
