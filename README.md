# makesecretlib

The shared library used by the [makesecret](https://gitlab.com/ghost-in-the-zsh/makesecret) and [makesecret-qt](https://gitlab.com/ghost-in-the-zsh/makesecret-qt) programs. This library is declared as a dependency in these projects and is intended for development purposes.


## Requirements

* Python v3.6.5 or later
* Python `requests` module
* A GNU+Linux shell or Windows command prompt

The `requests` module dependency is automatically installed when installing this program using PIP.


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE).
