# https://semver.org/
VERSION = '0.1.1'

# Number of guesses per second assumed during brute-force analysis.
ATTEMPTS_PER_SECOND = 10**9

# Bits of entropy needed to consider a result good enough, i.e. secure.
MINIMUM_ENTROPY_BITS = 80

# Digits of precision when showing time analysis results.
DECIMAL_PRECISION = 4

# Number of words chosen for passphrases
PASSPHRASE_WORD_COUNT = 8

# Number of characters chosen for passwords
PASSWORD_CHAR_COUNT = 16
