from typing import Text


class Secret:
    '''A class to store the result returned by a provider.'''

    def __init__(self, result: Text, entropy: int):
        self._result = result
        self._entropy = entropy

    @property
    def result(self) -> Text:
        return self._result

    @property
    def entropy(self) -> int:
        return self._entropy

    def __repr__(self):
        return "<{}: result='{}' entropy='{:,}'>".format(
            self.__class__.__name__,
            '*' * len(self._result),
            self._entropy
        )

    def __str__(self):
        return self.result


class SecretStrengthMeta:
    '''Class that encapsulates a Secret's strength metadata.'''

    def __init__(self, *,
        entropy_base: int,
        entropy_bits: float,
        entropy_bits_total: float,
        entropy_bits_effective: float,
        entropy_bits_minimum: float,
        attacks_per_second: int,
        crack_total_count: int,
        crack_secs: float,
        crack_hours: float,
        crack_days: float,
        crack_weeks: float,
        crack_years: float,
        crack_decades: float,
        crack_centuries: float,
        crack_millenia: float,
        crack_millions: float,
        crack_billions: float,
        precision: int,
    ):
        self.entropy_base = entropy_base
        self.entropy_bits = entropy_bits
        self.entropy_bits_total = entropy_bits_total
        self.entropy_bits_effective = entropy_bits_effective
        self.entropy_bits_minimum = entropy_bits_minimum
        self.attacks_per_second = attacks_per_second
        self.crack_total_count = crack_total_count
        self.crack_secs = crack_secs
        self.crack_hours = crack_hours
        self.crack_days = crack_days
        self.crack_weeks = crack_weeks
        self.crack_years = crack_years
        self.crack_decades = crack_decades
        self.crack_centuries = crack_centuries
        self.crack_millenia = crack_millenia
        self.crack_millions = crack_millions
        self.crack_billions = crack_billions
        self.precision = precision


class SecretBreachMeta:
    '''Class that encapsulates a Secret's known breaches metadata.'''

    def __init__(self, *,
        secret_text: Text,
        sha1hash: Text,
        request_url: Text,
        candidates_found: int,
        known_breaches_count: int
    ):
        self.secret_text = secret_text
        self.sha1hash = sha1hash
        self.request_url = request_url
        self.candidates_found = candidates_found
        self.known_breaches_count = known_breaches_count
