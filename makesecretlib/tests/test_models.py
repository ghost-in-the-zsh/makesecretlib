
from unittest import TestCase

from makesecretlib.models import \
    Secret, \
    SecretStrengthMeta, \
    SecretBreachMeta


class SecretTestCase(TestCase):

    def test_secret_result_matches(self):
        s = Secret('secret', 5)
        self.assertEqual(s.result, 'secret')

    def test_secret_entropy_matches(self):
        s = Secret('secret', 10)
        self.assertEqual(s.entropy, 10)

    def test_str_method_returns_result(self):
        s = Secret('secret', 5)
        self.assertEqual(str(s), 'secret')

    def test_repr_method_format_matches(self):
        s = Secret('secret', 1000)
        self.assertEqual(repr(s), "<Secret: result='******' entropy='1,000'>")


class SecretStrengthMetaTestCase(TestCase):

    def setUp(self):
        self._meta = SecretStrengthMeta(
            entropy_base=1,
            entropy_bits=2,
            entropy_bits_total=3,
            entropy_bits_effective=4,
            entropy_bits_minimum=5,
            attacks_per_second=6,
            crack_total_count=7,
            crack_secs=8,
            crack_hours=9,
            crack_days=10,
            crack_weeks=11,
            crack_years=12,
            crack_decades=13,
            crack_centuries=14,
            crack_millenia=15,
            crack_millions=16,
            crack_billions=17,
            precision=18
        )

    def tearDown(self):
        del self._meta

    def test_entropy_base_matches(self):
        self.assertEqual(self._meta.entropy_base, 1)

    def test_entropy_bits_matches(self):
        self.assertEqual(self._meta.entropy_bits, 2)

    def test_entropy_bits_total_matches(self):
        self.assertEqual(self._meta.entropy_bits_total, 3)

    def test_entropy_bits_effective_matches(self):
        self.assertEqual(self._meta.entropy_bits_effective, 4)

    def test_entropy_bits_minimum_matches(self):
        self.assertEqual(self._meta.entropy_bits_minimum, 5)

    def test_attacks_per_second_matches(self):
        self.assertEqual(self._meta.attacks_per_second, 6)

    def test_crack_total_count_matches(self):
        self.assertEqual(self._meta.crack_total_count, 7)

    def test_crack_secs_matches(self):
        self.assertEqual(self._meta.crack_secs, 8)

    def test_crack_hours_matches(self):
        self.assertEqual(self._meta.crack_hours, 9)

    def test_crack_days_matches(self):
        self.assertEqual(self._meta.crack_days, 10)

    def test_crack_weeks_matches(self):
        self.assertEqual(self._meta.crack_weeks, 11)

    def test_crack_years_matches(self):
        self.assertEqual(self._meta.crack_years, 12)

    def test_crack_decades_matches(self):
        self.assertEqual(self._meta.crack_decades, 13)

    def test_crack_centuries_matches(self):
        self.assertEqual(self._meta.crack_centuries, 14)

    def test_crack_millenia_matches(self):
        self.assertEqual(self._meta.crack_millenia, 15)

    def test_crack_millions_matches(self):
        self.assertEqual(self._meta.crack_millions, 16)

    def test_crack_billions_matches(self):
        self.assertEqual(self._meta.crack_billions, 17)

    def test_precision_matches(self):
        self.assertEqual(self._meta.precision, 18)


class SecretBreachMetaTestCase(TestCase):

    def setUp(self):
        self._meta = SecretBreachMeta(
            secret_text='secret',
            sha1hash='secret-hash', # totally
            request_url='https://example.org',
            candidates_found=1,
            known_breaches_count=2
        )

    def tearDown(self):
        del self._meta

    def test_crack_centuries_matches(self):
        self.assertEqual(self._meta.secret_text, 'secret')

    def test_crack_millenia_matches(self):
        self.assertEqual(self._meta.sha1hash, 'secret-hash')

    def test_crack_millions_matches(self):
        self.assertEqual(self._meta.request_url, 'https://example.org')

    def test_crack_billions_matches(self):
        self.assertEqual(self._meta.candidates_found, 1)

    def test_precision_matches(self):
        self.assertEqual(self._meta.known_breaches_count, 2)
