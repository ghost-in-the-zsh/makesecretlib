
from unittest import TestCase

from string import ascii_letters, digits, punctuation

from makesecretlib.models import Secret
from makesecretlib.providers import PassphraseProvider, PasswordProvider


class PasswordProviderTestCase(TestCase):

    PASSWORD_LENGTH  = 16
    DEFAULT_CHOICE_POOL_SIZE = len(ascii_letters + digits)
    SPECIAL_CHOICE_POOL_SIZE = len(ascii_letters + digits + punctuation)

    def setUp(self):
        self._provider = PasswordProvider(
            user_choice_cnt=PasswordProviderTestCase.PASSWORD_LENGTH,
            with_special=False
        )

    def test_password_length_matches(self):
        secret = self._provider.generate()
        self.assertEqual(
            len(secret.result),
            PasswordProviderTestCase.PASSWORD_LENGTH
        )

    def test_default_password_choice_pool_size_matches(self):
        secret = self._provider.generate()
        self.assertEqual(
            secret.entropy,
            PasswordProviderTestCase.DEFAULT_CHOICE_POOL_SIZE
        )

    def test_special_password_choice_pool_size_matches(self):
        secret = PasswordProvider(
            user_choice_cnt=PasswordProviderTestCase.PASSWORD_LENGTH,
            with_special=True
        ).generate()
        self.assertEqual(
            secret.entropy,
            PasswordProviderTestCase.SPECIAL_CHOICE_POOL_SIZE
        )


class PassphraseProviderTestCase(TestCase):

    PHRASE_WORD_COUNT = 5
    WORDS_LIST_LENGTH = 7776

    def setUp(self):
        self._provider = PassphraseProvider(
            wordslist_file='makesecretlib/data/eff_wordlist.db',
            user_choice_cnt=PassphraseProviderTestCase.PHRASE_WORD_COUNT,
            capitalize_words=False,
            with_numbers=False
        )

    def test_passphrase_word_count_matches(self):
        secret = self._provider.generate()
        self.assertEqual(
            len(secret.result.split(' ')),
            PassphraseProviderTestCase.PHRASE_WORD_COUNT
        )

    def test_passphrase_words_list_length_matches(self):
        secret = self._provider.generate()
        self.assertEqual(
            secret.entropy,
            PassphraseProviderTestCase.WORDS_LIST_LENGTH
        )
