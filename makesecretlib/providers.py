
import argparse as ap

from secrets import choice
from string import ascii_letters, digits, punctuation
from abc import ABCMeta, abstractmethod
from typing import Text

from makesecretlib.models import Secret


class _SecretProvider(metaclass=ABCMeta):
    '''ABC for providers.'''

    @abstractmethod
    def generate(self) -> Secret:
        raise NotImplementedError()


class PasswordProvider(_SecretProvider):
    '''A generator for passwords based on ascii symbols.'''

    def __init__(self, *, user_choice_cnt: int, with_special: bool):
        self._user_choice_count = user_choice_cnt
        self._with_special = with_special

    def generate(self) -> Secret:
        '''Generate a password using a set of alphanumeric symbols.

        The alphanumeric set includes lower/upper-case letters, numbers,
        and (optionally) special/punctuation symbols.
        '''
        options = ascii_letters + digits
        if self._with_special:
            options += punctuation

        return Secret(
            ''.join(choice(options) for i in range(self._user_choice_count)),
            len(options)
        )


class PassphraseProvider(_SecretProvider):
    '''A generator for word-based passphrases from word lists.'''

    def __init__(self, *, wordslist_file: str, user_choice_cnt: int, capitalize_words: bool, with_numbers: bool):
        self._wordslist_file = wordslist_file
        self._user_choice_cnt = user_choice_cnt
        self._capitalize_words = capitalize_words
        self._with_numbers = with_numbers

    def generate(self) -> Secret:
        '''Generate a passphrase from a list of common words.

        The words list comes from the Electronic Frontier Foundation
        (EFF) and it's made up of common words, so it should be fairly
        easy to remember.
        '''
        with open(self._wordslist_file, 'r') as words:
            options = [word.strip() for word in words]

        random_words = ' '.join(choice(options) for i in range(self._user_choice_cnt))
        base_entropy = len(options)

        # this adds no entropy b/c capitalization is not performed randomly,
        # but it helps with systems that enforce capitalization rules
        if self._capitalize_words:
            random_words = ' '.join(word.capitalize() for word in random_words.split())

        # this adds entropy; for every randomly chosen word, we randomly choose
        # a digit to append to it; there're now 10 more ways each word could've been
        # randomly chosen, which is the same as if the original list had been 10
        # times longer; the entropy changes as follows: log(A) + log(B) == log(A*B),
        # noting that the raw/base entropy is multiplied because it's not yet in bits
        if self._with_numbers:
            random_list = random_words.split()
            random_words = ' '.join(word + choice(digits) for word in random_list)
            base_entropy *= len(digits)

        return Secret(
            random_words,
            base_entropy
        )
