import hashlib

from abc import ABCMeta, abstractmethod
from typing import Text, Tuple
from os import linesep as eol
from math import log2
from http import HTTPStatus
from http.client import responses

import requests
from requests.exceptions import RequestException

from makesecretlib.models import \
    Secret, \
    SecretStrengthMeta, \
    SecretBreachMeta


class _Analyzer(metaclass=ABCMeta):
    '''ABC for analyzers.'''

    @abstractmethod
    def analyze(self, **kwargs):
        raise NotImplementedError()


class StrengthAnalyzer(_Analyzer):
    '''Analyzes the strength of a secret based on its entropy.'''

    def analyze(
        self, *,
        secret: Secret,
        user_choices_count: int,
        attacks_per_second: int,
        entropy_bits_minimum: int,
        precision: int
    ) -> SecretStrengthMeta:
        entropy_base = secret.entropy
        entropy_bits = log2(entropy_base)
        entropy_bits_total = entropy_bits * user_choices_count
        entropy_bits_effective = entropy_bits_total - 1
        tries_to_crack = int(2**entropy_bits_effective)  # average

        # these are only averages
        crack_secs      = tries_to_crack / attacks_per_second
        crack_hours     = crack_secs  / 3600
        crack_days      = crack_hours / 24
        crack_weeks     = crack_days  / 7
        crack_years     = crack_days  / 365.25
        crack_decades   = crack_years / 10
        crack_centuries = crack_years / 10**2
        crack_millenia  = crack_years / 10**3
        crack_millions  = crack_years / 10**6
        crack_billions  = crack_years / 10**9

        return SecretStrengthMeta(
            entropy_base=entropy_base,
            entropy_bits=entropy_bits,
            entropy_bits_total=entropy_bits_total,
            entropy_bits_effective=entropy_bits_effective,
            entropy_bits_minimum=entropy_bits_minimum,
            attacks_per_second=attacks_per_second,
            crack_total_count=tries_to_crack,
            crack_secs=crack_secs,
            crack_hours=crack_hours,
            crack_days=crack_days,
            crack_weeks=crack_weeks,
            crack_years=crack_years,
            crack_decades=crack_decades,
            crack_centuries=crack_centuries,
            crack_millenia=crack_millenia,
            crack_millions=crack_millions,
            crack_billions=crack_billions,
            precision=precision
        )


class BreachAnalyzer(_Analyzer):
    '''Analyzes whether a secret has been exposed before or not.'''

    def analyze(self, secret: Secret) -> SecretBreachMeta:
        sha1hash = hashlib.sha1(secret.result.encode('utf-8')).hexdigest()

        # we only send a 5 character prefix of the hash and *never* the full hash;
        # this protects the secret being verified and prevents identification of the
        # original secret by reverse-lookup on the remote side;
        # see https://en.wikipedia.org/wiki/K-anonymity
        # or https://www.youtube.com/watch?v=hhUb5iknVJs if you don't want to read
        local_prefix = sha1hash[:5]
        local_suffix = sha1hash[5:].upper() # API replies are upper-case

        remote_host = 'https://api.pwnedpasswords.com'
        request_url = f'{remote_host}/range/{local_prefix}'
        try:
            response = requests.get(request_url)
        except RequestException:
            raise RuntimeError(
                f'Failed to contact {remote_host}. Your network connection ' \
                '(or remote host) may be down or having other temporary '\
                'issues. Try again later.'
            )

        if response.status_code != HTTPStatus.OK.value:
            status = responses[response.status_code]
            raise RuntimeError(f'Failed to fetch {request_url}: {status}')

        # using a list instead of a generator here makes it easier to
        # later get the number of results returned by the remote API;
        # the average results range is 478 suffixes, so this should be fine
        hashes = [line.split(':') for line in response.text.splitlines()]
        matches = next((int(count) for suffix,count in hashes if local_suffix == suffix), 0)

        return SecretBreachMeta(
            secret_text=secret.result,
            sha1hash=sha1hash,
            request_url=request_url,
            known_breaches_count=matches,
            candidates_found=len(hashes)
        )
